﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Gitignore
{
    class Program
    {
        static void Main(string[] args)
        {
            var ignore = Settings.Get();

            List<string> sb = new List<string>();
            foreach (var dir0 in Directory.GetDirectories(Environment.CurrentDirectory))
            {
                if (ignore.DirLevel0.Contains(new DirectoryInfo(dir0).Name.ToLower()))
                    sb.Add(dir0.ToGitPath());

                foreach (var dir1 in Directory.GetDirectories(dir0))
                {
                    if (ignore.DirLevel1.Contains(new DirectoryInfo(dir1).Name.ToLower()))
                        sb.Add(dir1.ToGitPath());
                }

                //Add Extensions
                foreach (var fileExtension in ignore.File)
                    if (Directory.GetFiles(dir0).Any(filePath => new FileInfo(filePath).Extension == fileExtension))
                    {
                        var gitignoreLine = (dir0 + "/*" + fileExtension).ToGitPath();
                        if (!sb.Contains(gitignoreLine))
                            sb.Add(gitignoreLine);
                    }
            }

            File.WriteAllLines(Path.Combine(Environment.CurrentDirectory, ".gitignore"), sb.ToArray());
        }
    }
}
