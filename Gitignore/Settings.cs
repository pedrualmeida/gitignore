﻿using Newtonsoft.Json;
using System;

namespace Gitignore
{
    public class Settings
    {
        public string[] DirLevel0 { get; set; }
        public string[] DirLevel1 { get; set; }
        public string[] File { get; set; }

        public static Settings Get()
        {
            var executablePath = System.Reflection.Assembly.GetEntryAssembly().Location;
            var configFilePath = executablePath.Replace(".dll", ".json").Replace(".exe", ".json");
            Settings responseSettings = null;
            if (System.IO.File.Exists(configFilePath))
            {
                var configFileContent = System.IO.File.ReadAllText(configFilePath);
                try
                {
                    responseSettings = JsonConvert.DeserializeObject<Settings>(configFileContent);
                }
                catch (Exception ex)
                {
                    responseSettings = null;
                }
            }

            if (responseSettings == null || responseSettings.DirLevel0 == null || responseSettings.DirLevel1 == null || responseSettings.File == null)
            {
                responseSettings = new Settings()
                {
                    DirLevel0 = new string[] { ".vs", "packages" },
                    DirLevel1 = new string[] { "obj", "bin", ".vs", ".user", "packages" },
                    File = new string[] { ".user", ".vspscc" }
                };
                System.IO.File.WriteAllText(configFilePath, JsonConvert.SerializeObject(responseSettings, Formatting.Indented));
            }

            return responseSettings;
        }
    }
}
