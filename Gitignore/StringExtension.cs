﻿using System;

namespace Gitignore
{
    public static class StringExtension
    {
        public static string ToGitPath(this string str)
        {
            return str.Replace(Environment.CurrentDirectory, "").Replace('\\', '/');
        }
    }
}
